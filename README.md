# Matching Model #

This package provides code which solves a matching model with heterogeneous, large firms. Firms are ex-ante identical but experience swings in idiosyncratic demand and hence choose different recruitment and redundancy policies. The result is a non-degenerate firm-size distribution.

The code is written in [Scala](http://www.scala-lang.org) and uses the [ModelSolver Toolkit](http://modelsolver.bitbucket.org).

The model is discussed in chapter 4 of the thesis.

## The Model ##

All code to solve the model is implemented in 

~~~~
src/main/scala/com/meliorbis/economics/matching/quadraticCosts/demandShocks/SignUpAbsolute
~~~~

The slightly cryptic name indicates that surplus sharing is achieved using a *sign-up* bonus, and costs are calculated from *absolute* vancancy and redundancy postings, rather than relative to size.

It can be viewed on-line [here](https://bitbucket.org/modelsolver/matching-model/src/6332cff2411816c219ae65e66b2b069e381c0b43/src/main/scala/com/meliorbis/economics/matching/quadraticCosts/demandShocks/SignUpAbsolute.scala?at=master&fileviewer=file-view-default).
## Solving Models & Evaluating Results in the Pre-Compiled Package

### Solving
If you downloaded the pre-compiled package or already build the package as specific below, change directory to 

~~~~
src/main/R
~~~~

and run *solveModels.R*. It can be run non-interactively to solve all models/ For example, on a unix-like system:

~~~~
R --save < solveModels.R
~~~~

will calculate all the solutions and place results in the directory *Solutions*.

It can also executed in an R console and will allow selection of the model to solve.

### Evaluating Results

Once all models are solved, *prepareResults.R* calculates the statistics and generates tables and graphs used in the paper & chapter 4.

~~~~
R --save < prepareResults.R
~~~~

## Compiling and Building

To compile the code and build the runnable package *maven* must be installed. In the root directory of the project call:

~~~~
mvn clean package
~~~~