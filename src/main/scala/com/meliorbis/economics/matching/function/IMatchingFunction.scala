package com.meliorbis.economics.matching.function

trait IMatchingFunction {
  
  /**
   * The number of matches under the assumption of the given level of employment
   * and vacancies
   * 
   * @param L The proportion of the labour force in employment
   * @param V The number of vacancies posted
   * 
   * @return The number of matches
   */
  def matches(θ: Double) : Double
  
  /**
   * The number of matches under the assumption of the given level of employment
   * and vacancies
   * 
   * @param L The proportion of the labour force in employment
   * @param V The number of vacancies posted
   * 
   * @return The number of matches
   */
  def matches(L : Double,V : Double) : Double = matches(V/(1-L))
  
  /**
   * The probability of a job seeker finding a job - commonly denoted as f(θ)
   * 
   * @param θ Market tightness
   * 
   * @return The job finding rate
   */
  def jobFindingRate(θ: Double) : Double
  
  /**
   * The probability of a job seeker finding a job - commonly denoted as f(θ)
   * 
   * @param L The proportion of the labour force in employment
   * @param V The number of vacancies posted
   * 
   * @return The job finding rate
   */
  def jobFindingRate(L : Double,V : Double) : Double = jobFindingRate(V/(1-L))
  
  /**
   * The probability of a posted vacancy being filled - commonly denoted as q(θ)
   * 
   * @param θ Market tightness
   * 
   * @return The rate at which posted vacancies are filled
   */
  def vacancyFillingRate(θ: Double) : Double
  
  /**
   * The probability of a posted vacancy being filled - commonly denoted as q(θ)
   * 
   * @param L The proportion of the labour force in employment
   * @param V The number of vacancies posted
   * 
   * @return The rate at which posted vacancies are filled
   */
  def vacancyFillingRate(L : Double,V : Double) : Double = vacancyFillingRate(V/(1-L))
}