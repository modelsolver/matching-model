/**
 *
 */
package com.meliorbis.economics.matching.function

/**
 * Implements the matching function introduced in Den Haan, Ramey and Watson (2000).
 * 
 * @author Tobias Grasl
 */
class DRWMatchingFunction(val iota: Double) extends IMatchingFunction {

  // Note that this is already accounting for its use as the denominator
  private val _iotaInv = -1d/iota
  
  override final def matches(θ : Double): Double = {
    throw new UnsupportedOperationException("Can't calculate matches from θ with DRW matching")
  }
  
  override final def matches(L : Double, V : Double): Double = {
    V * vacancyFillingRate(V/(1-L))
  }

  
  override final def jobFindingRate(θ: Double): Double = {
     θ * vacancyFillingRate(θ)
  }


  override final def vacancyFillingRate(θ: Double): Double = {
    Math.pow(1 + Math.pow(θ, iota),_iotaInv)
  }
}