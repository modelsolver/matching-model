package com.meliorbis.economics.matching.quadraticCosts.demandShocks

import java.io.File

import scala.collection.JavaConversions.mapAsJavaMap
import scala.collection.mutable.ListBuffer

import org.apache.commons.cli.{ CommandLine, Options }
import org.scalactic.Tolerance.convertNumericToPlusOrMinusWrapper
import org.scalactic.TripleEquals.convertToEqualizer

import com.meliorbis.economics.aggregate.derivagg.{ DerivAggCalcState, DerivativeAggregationSolver }
import com.meliorbis.economics.individual.egm.EGMIndividualProblemSolverWithControls
import com.meliorbis.economics.infrastructure.{ AbstractModelWithControls, ModelConfigBase }
import com.meliorbis.economics.infrastructure.simulation.{ DiscretisedDistribution, DiscretisedDistributionSimulator, SimState, SimulationObserver, SimulationResults, TransitionRecord }
import com.meliorbis.economics.matching.function.DRWMatchingFunction
import com.meliorbis.economics.matching.quadraticCosts.demandShocks.SignUpAbsolute.{ MatchingDASolver, Model, SimpleQuadraticCostsSolver }
import com.meliorbis.economics.model.{ AbstractStateWithControls, AggregateFixedPointState, ModelAndConfigValueDelegate, ModelRunner }
import com.meliorbis.economics.scala.ModelHelpers.arrayChangedConv
import com.meliorbis.numerics.DoubleArrayFactories.{ createArray, createArrayOfSize }
import com.meliorbis.numerics.IntArrayFactories.{ createIntArray, createIntArrayOfSize }
import com.meliorbis.numerics.convergence.{ DoubleCriterion, ProgressDisplayingCallback }
import com.meliorbis.numerics.fixedpoint.FixedPointValueDelegate
import com.meliorbis.numerics.generic.MultiDimensionalArray
import com.meliorbis.numerics.generic.impl.IntegerArray
import com.meliorbis.numerics.generic.primitives.impl.DoubleArrayFunctions._
import com.meliorbis.numerics.generic.primitives.impl.Interpolation
import com.meliorbis.numerics.generic.primitives.impl.Interpolation.{ interp, interpolateFunction, params, spec }
import com.meliorbis.numerics.io.{ NumericsReader, NumericsWriter }
import com.meliorbis.numerics.markov.DiscreteStochasticProcessFactory
import com.meliorbis.numerics.scala.DoubleArray._
import com.meliorbis.utils.{ Pair, Utils }
import com.meliorbis.economics.aggregate.AggregateProblemSolver
import com.meliorbis.economics.individual.IndividualProblemSolver
import com.meliorbis.economics.aggregate.ks.KrusellSmithSolver

/**
 * - Sign up bonus
 * - Quadratic Costs, based on '''absolute''' vacancy & redundancy postings, 
 * rather than relative to size
 * - Demand Shocks
 * - Wages determined one period in advance
 */
object SignUpAbsolute {

  object Opts {
    val NOAGG = "noAgg"
    val NOAGGALT = "noAggAlt"
    val NOEXIT = "noExit"
    val HIGHTAX = "highTax"
    val INITDIST = "initDist"
    val TRANSITION = "transition"
  }

  def main(args: Array[String]) {
    println(s"Running ${this.getClass.getName}")

    Runner.run(args)
  }
  
  object SteadyStates {
    val highTax = (0.8863874284242546,
            0.08869724385663504,
            0.02735725627145646,
            2.781739154850565,
            0.009718407,
            95.43368)
    val lowTax = (1-0.10747289,
            0.09147023,
            0.02841023,
            2.76369891,
            0.009627019,
            95.43368)
  }

  object Runner extends ModelRunner[Model, Config, State] {

    val (l, v, v2, ybyL, entry, entryVal) = SteadyStates.highTax

    def initialiseState(model: Model, commandLine: CommandLine): State = {

      val state = getSolver().readState(model, new File(commandLine.getOptionValue("initialState")))

      val aggSize = state.getAggregateTransition().size().clone()
      val midLevel = state.getAggregateTransition().size()(0) / 2
      aggSize(0) = 1
      val indSize = state.getIndividualPolicy().size.clone()
      indSize(2) = 1

      val lArray = createArray(Utils.sequence(l - .04, Math.min(l + .04, .99), 5): _*)
      

      state.setAggregateTransition(createArrayOfSize(aggSize: _*) << interp(state.getAggregateTransition(),spec(0,lArray,l)))
      state.setIndividualPolicy(createArrayOfSize(indSize: _*) <<
          interp(state.getIndividualPolicy(),spec(2,lArray,l)))
      state.setIndividualControlsPolicy(createArrayOfSize(indSize: _*) << interp(state.getIndividualControlsPolicy(),spec(2,lArray,l)))

      state
    }
    override def runConfig(config: Config, commandLine: CommandLine): Unit = {

      if (!commandLine.hasOption(Opts.TRANSITION)) {
        super.runConfig(config, commandLine)
      } else {

        withSimToConv(config, commandLine)
        //fixedLengthPath(config, commandLine)

      }
    }
    
    def fixedLengthPath(config: Config, commandLine: CommandLine): Unit = {
      
      val model = instantiateModel(getModelClass(), config)
      model.initialise()

      var state = initialiseState(model, commandLine)

      var error = 1d
      
      val periods = 150
      var adjustment = createArrayOfSize(periods) << 1d
      
//      for( i <- 0 until periods) {
//        adjustment(i) = (.99^i)
//      }
      
      
      var states = createArrayOfSize(periods,2)\1 << (l, ybyL)
      var controls = createArrayOfSize(periods,2)\1 << (v, v2)
      
      var iterations = 0
      
      val cb = new ProgressDisplayingCallback
      
      while (error > 1e-6) {
        
        var period = periods 
        
        // Reinitialise the state
        state = initialiseState(model, commandLine)

        val expectedStates = model.createAggregateExpectationGrid(2)
        expectedStates << (l, ybyL)

        state.setExpectedAggregateStates(expectedStates)

        val expectedControls = model.createAggregateExpectationGrid(2)
        expectedControls << (v, v2)

        state.setExpectedAggregateControls(expectedControls)

        val statePolicies = new Array[DoubleArray](periods)
        val controlPolicies = new Array[DoubleArray](periods)
        
        // Iterate backward, solving the firms' problem under the assumptions
         while (period > 0) {

          period = period - 1

          config.getAggregateEndogenousStates.get(0)(0) = states(period, 0)
          config.getAggregateEndogenousStates.get(1)(0) = states(period, 1)

          config.getAggregateControls.get(0)(0) = controls(period, 0)
          config.getAggregateControls.get(1)(0) = controls(period, 1)

          var localModel = instantiateModel(classOf[Model], config)

          localModel.initialise()

          assert(localModel.aggQ(0) == config.matchingFn.vacancyFillingRate(states(period, 0),
              controls(period, 0)), "Model Q does not match input values")
              
          // Create a new one to init with correct aggregates etc
          var solver = new SimpleQuadraticCostsSolver(model, config)

          solver.initialise(state)

          solver.performIteration(state)

          // Now the current become expectations for the next iteration
          expectedStates << states(period, $)
          expectedControls << controls(period, $)

          // Remember the individual policies for the next run
          statePolicies(period) = state.getIndividualPolicy
          controlPolicies(period) = state.getIndividualControlsPolicy
        }
        
        // Simulate those policies and generate the sequence of states
        val (newStates, newControls) = simulatePolicies(model, state, statePolicies, controlPolicies)
        
        val statesDiff = newStates - states
        val controlsDiff = newControls - controls
        
        error = maximumRelativeDifference(states, newStates)
        
        // Update for the next iteration
        states += (statesDiff\(0) * adjustment)
        controls += (controlsDiff\(0) * adjustment)
        
        
        iterations = iterations + 1

        println(f"Iteration: $iterations%3d, Error: $error%.2e")
        //cb.notify(new Pair(None, new DoubleCriterion(error)), iterations)
        if(iterations == 1) adjustment << .01
        if(error < 2e-3) adjustment << .1
      }
    }

    def progressivePath(config: Config, commandLine: CommandLine): Unit = {
      val model = instantiateModel(getModelClass(), config)
      model.initialise()

      var state = initialiseState(model, commandLine)

      var statePolicies = new Array[DoubleArray](0)
      //          statePolicies(0) = state.getIndividualPolicy

      var controlPolicies = new Array[DoubleArray](0)
      //         controlPolicies(0) = state.getIndividualControlsPolicy

      var error = 1d
      
      var lastStates = createArrayOfSize(1,2) << (l, ybyL)
      var lastControls = createArrayOfSize(1,2) << (v, v2)
      
      var iterations = 0
      
      val cb = new ProgressDisplayingCallback
      
      while (error > 1e-6) {
        
        iterations = iterations + 1
        
        cb.notify(new Pair(None, new DoubleCriterion(error)), iterations)
        val (newStates, newControls) = simulatePolicies(model, state, statePolicies, controlPolicies)

        val solvePeriods = newStates.size()(0)

        // Reinitialise the state
        state = initialiseState(model, commandLine)

        val expectedStates = model.createAggregateExpectationGrid(2)
        expectedStates << (l, ybyL)

        state.setExpectedAggregateStates(expectedStates)

        val expectedControls = model.createAggregateExpectationGrid(2)
        expectedControls << (v, v2)

        state.setExpectedAggregateControls(expectedControls)

        statePolicies = new Array[DoubleArray](solvePeriods)
        controlPolicies = new Array[DoubleArray](solvePeriods)

        val limit = (delta: Double) => ((oldVal: Double, newVal: Double) => {
                  val (plus, minus) = (oldVal * (1+delta), oldVal*(1-delta))
                  
                  val (max, min) = if(plus > minus) (plus, minus) else (minus, plus)
                  
                  if(newVal > max) max else if(newVal < min) min else newVal
                })
        for( period <- 0 until newStates.size()(0)) {
          
          if(period < lastStates.size()(0)) {
            newStates(period,$) << (lastStates(period,$)~newStates(period,$)) -> limit(1e-3)
            newControls(period,$) << (lastControls(period,$)~newControls(period,$)) -> limit(1e-3)
          } else {
            newStates(period,$) << (lastStates(lastStates.size()(0) - 1,$)~newStates(period,$)) -> limit(1e-3)
            newControls(period,$) << (lastControls(lastStates.size()(0) - 1,$)~newControls(period,$)) -> limit(1e-3)            
          }
        }
        
        lastStates = newStates
        lastControls = newControls
        
        var period = solvePeriods
        

        while (period > 0) {

          period = period - 1

          config.getAggregateEndogenousStates.get(0)(0) = newStates(period, 0)
          config.getAggregateEndogenousStates.get(1)(0) = newStates(period, 1)

          config.getAggregateControls.get(0)(0) = newControls(period, 0)
          config.getAggregateControls.get(1)(0) = newControls(period, 1)
          config.getAggregateControls.get(2)(0) = newControls(period, 2)

          var localModel = instantiateModel(classOf[Model], config)

          localModel.initialise()

          var solver = localModel.getIndividualSolverInstance

          solver.initialise(state)

          solver.performIteration(state)

          // Now the current become expectations for the next iteration
          expectedStates << newStates(period, $)
          expectedControls << newControls(period, $)

          // Remember the individual policies for the next run
          statePolicies(period) = state.getIndividualPolicy
          controlPolicies(period) = state.getIndividualControlsPolicy
        }

      }
    }

    def simulatePolicies(model: Model,
      state: State,
      statePolicies: Array[DoubleArray],
      controlPolicies: Array[DoubleArray]): (DoubleArray, DoubleArray) = {
      val simulator = getSimulator()

      val initialDist = model.getConfig.getInitialSimState().asInstanceOf[DiscretisedDistribution]

      var currentDist = initialDist

      val shocks = createIntArray(0)

      var error = 1d

      var indError = 1d

      val policyCount = statePolicies.length

      val results = new SimulationResults[DiscretisedDistribution, Integer]

      var lastStates: Option[DoubleArray] = None //(createArray(0))
      var lastControls: Option[DoubleArray] = None
      var lastDist = initialDist

      for (period <- 0 until policyCount) {

        state.setIndividualPolicy(statePolicies(period))
        state.setIndividualControlsPolicy(controlPolicies(period))

        val record = simulator.simulateTransition(lastDist, model, state, shocks, shocks)

        lastStates = Some(record.getStates)
        lastControls = Some(record.getControls)
        lastDist = record.getResultingDistribution

        results.addPeriod(shocks, lastStates.get, lastControls.get)
      }

//      val record = simulator.simulateTransition(lastDist, model, state, shocks, shocks)
//
//      val currentStates = record.getStates
//      val currentControls = record.getControls
//
//      if (lastStates.isEmpty || maximumRelativeDifference(lastStates.get, currentStates) > 1e-6) {
//        results.addPeriod(shocks, currentStates, currentControls)
//      }

      val aggregateStates = results.getStates
      val aggregateControls = results.getControls

      debugWriteArray(aggregateStates, "transStates")
      debugWriteArray(aggregateControls, "transControls")
      debugWriteArray(lastDist._density, "lastDens")

      val bound = (target: Double, delta: Double) => 
        (value : Double) => value.max(target*(1d-delta)).min(target*(1d+delta))
      
      //aggregateStates($, 0) ->= bound(l, .02)
      //aggregateStates($, 1) ->= bound(ybyL, .02)
      
      //aggregateControls($, 0) ->= bound(v,.04)
      //aggregateControls($, 2) ->= bound(v2,.04)
      

      (aggregateStates, aggregateControls)
    }

    def withSimToConv(config: Config, commandLine: CommandLine): Unit = {
      val model = instantiateModel(getModelClass(), config)
      model.initialise()

      var state = initialiseState(model, commandLine)

      var statePolicies = new Array[DoubleArray](1)
      statePolicies(0) = state.getIndividualPolicy

      var controlPolicies = new Array[DoubleArray](1)
      controlPolicies(0) = state.getIndividualControlsPolicy

      val steadyStates = createArray(l, ybyL)
      val steadyCtrsl = createArray(v, v2)

      var states: DoubleArray = createArrayOfSize(1, steadyStates.numberOfElements) << steadyStates
      var controls: DoubleArray = createArrayOfSize(1, steadyCtrsl.numberOfElements) << steadyCtrsl

      var solvePeriods = 0

      var error = 1d
      var maxChange = 1d

      val updateOp =
        (delta: Double) => (oldVal: Double, newVal: Double) => oldVal + delta * (newVal - oldVal)
      //          (delta: Double) => ((oldVal: Double, newVal: Double) => {
      //            val (plus, minus) = (oldVal * (1+delta), oldVal*(1-delta))
      //            
      //            val (max, min) = if(plus > minus) (plus, minus) else (minus, plus)
      //            
      //            if(newVal > max) max else if(newVal < min) min else newVal
      //          })

      val indSolver = model.getIndividualSolverInstance().asInstanceOf[SimpleQuadraticCostsSolver]
      indSolver.initialise(state)

      var lastPeriods = 0
      var iterations = 0
      
      while (error > 1e-6) {
        iterations = iterations + 1
        
        val (newStates, newControls) =
          simulateToConvergence(model, state, steadyStates, statePolicies, controlPolicies)

        val newPeriods = newStates.size()(0)

        error = Math.abs(newPeriods - lastPeriods)

        lastPeriods = newPeriods

        solvePeriods = solvePeriods.max(newPeriods)

        val solveStates = createArrayOfSize(solvePeriods, config.getAggregateEndogenousStateCount)
        val solveControls = createArrayOfSize(solvePeriods, config.getAggregateControlCount)

        //var period = 0
        //val factor = if(iterations == 1) Utils.repeatArray(1d, solvePeriods) else Utils.sequence(maxChange, maxChange/100, solvePeriods)
        val factor = if(iterations != 1 && error > 1e-3) Utils.sequence(maxChange,1e-3, solvePeriods) else Utils.repeatArray(maxChange, solvePeriods)
          
        for (period <- 0 until solvePeriods) {

          var (lastItStates, lastItControls) = {
            if (period >= states.size()(0)) {
              //  (steadyStates, steadyCtrsl)
              (states(states.size()(0) - 1, $), controls(controls.size()(0) - 1, $))
            } else {
              (states(period, $), controls(period, $))
            }
          }

          var (newItStates, newItControls) = {
            if (period >= newStates.size()(0)) {
        //      (steadyStates, steadyCtrsl)
              (newStates(newStates.size()(0) - 1, $), newControls(newControls.size()(0) - 1, $))
            } else {
              (newStates(period, $), newControls(period, $))
            }
          }

          error = maximumRelativeDifference(newItStates, lastItStates).max(
            maximumRelativeDifference(newItControls, lastItControls)).max(error)

          
           
          solveStates(period, $) << (lastItStates + (newItStates-lastItStates)*factor(period))
          solveControls(period, $) << (lastItControls + (newItControls-lastItControls)*factor(period))
        }

        if(iterations == 1) maxChange = .1
//        if(error < 1e-3) maxChange = 1e-1

        println(f"Iteration: $iterations%3d,  Path error: $error%.2e")

        debugWriteArray(solveStates, "solveStates")
        debugWriteArray(solveControls, "solveControls")
        states = solveStates
        controls = solveControls

        if (error > 1e-6) {
          // Reinitialise the state
          state = initialiseState(model, commandLine)

          val expectedStates = model.createAggregateExpectationGrid(2)
          expectedStates << (l, ybyL)

          state.setExpectedAggregateStates(expectedStates)

          val expectedControls = model.createAggregateExpectationGrid(2)
          expectedControls << (v, v2)

          state.setExpectedAggregateControls(expectedControls)

          statePolicies = new Array[DoubleArray](solvePeriods + 1)
          controlPolicies = new Array[DoubleArray](solvePeriods + 1)

          statePolicies(solvePeriods) = state.getIndividualPolicy
          controlPolicies(solvePeriods) = state.getIndividualControlsPolicy

          var period = solvePeriods

          while (period > 0) {

            period = period - 1

            config.getAggregateEndogenousStates.get(0)(0) = states(period, 0)
            config.getAggregateEndogenousStates.get(1)(0) = states(period, 1)

            config.getAggregateControls.get(0)(0) = controls(period, 0)
            config.getAggregateControls.get(1)(0) = controls(period, 1)

              
            var localModel = instantiateModel(classOf[Model], config)

            localModel.initialise()

            assert(localModel.aggQ(0) == config.matchingFn.vacancyFillingRate(states(period, 0),
              controls(period, 0)), "Model Q does not match input values")
            
            var solver = new SimpleQuadraticCostsSolver(localModel, config)

            solver.initialise(state)

            solver.performIteration(state)

            // Now the current become expectations for the next iteration
            expectedStates << states(period, $)
            expectedControls << controls(period, $)

            // Remember the individual policies for the next run
            statePolicies(period) = state.getIndividualPolicy
            controlPolicies(period) = state.getIndividualControlsPolicy
          }
        }

      }

      println("Done")
    }

    def simulateToConvergence(model: Model,
      state: State,
      steadyStates: DoubleArray,
      statePolicies: Array[DoubleArray],
      controlPolicies: Array[DoubleArray]): (DoubleArray, DoubleArray) = {
      val simulator = getSimulator()

      val initialDist = model.getConfig.getInitialSimState().asInstanceOf[DiscretisedDistribution]

      var currentDist = initialDist

      // Can only handle 1000 periods max
      var period = 0

      val shocks = createIntArray(0)

      var error = 1d

      var indError = 1d

      val policyCount = statePolicies.length

      val results = new SimulationResults[DiscretisedDistribution, Integer]

      var lastStates: Option[DoubleArray] = Option.empty
      var lastControls: Option[DoubleArray] = Option.empty

      while (error > 1e-7) {

        // Set the periods if they are ones we calculated, or default to the final policy
        if (period < policyCount) {
          state.setIndividualPolicy(statePolicies(period))
          state.setIndividualControlsPolicy(controlPolicies(period))
        }

        val record = simulator.simulateTransition(currentDist, model, state, shocks, shocks)

        val currentStates = record.getStates
        val currentControls = record.getControls

        results.addPeriod(shocks, currentStates, currentControls)

        if (!lastStates.isEmpty) {
          // Convergence means both the change and the distance from the expected steady state value
          // are small
          //          error = maximumRelativeDifference(currentStates, steadyStates).max(
          //              maximumRelativeDifference(currentStates, lastStates.get))

          if (period % 10 == 0) {
            indError = maximumRelativeDifferenceSpecial(currentDist._density,
              record.getResultingDistribution._density)
          }
          error = //maximumRelativeDifference(currentStates, steadyStates).max(
            maximumRelativeDifference(currentStates, lastStates.get)//)
              .max(maximumRelativeDifference(currentControls, lastControls.get)) //.max(indError)

        }

        lastStates = Some(currentStates)
        lastControls = Some(currentControls)

        period = period + 1
        currentDist = record.getResultingDistribution

        if (period % 100 == 0) {
          println(f"Period: $period, Error: $error%.2e, Ind. Error: $indError%.2e")
        }
      }

      println(s"Path converged in $period periods")

      val aggregateStates = results.getStates
      val aggregateControls = results.getControls

      debugWriteArray(aggregateStates, "transStates")
      debugWriteArray(aggregateControls, "transControls")

      aggregateStates($, 0) ->= (_.min(.99))

      (aggregateStates, aggregateControls)
    }

    override def createOptions(): Options =
      {
        val options = super.createOptions();

        options.addOption(Opts.INITDIST, true, "The initial sim state to be used")
        options.addOption(Opts.NOAGG, false, "Indicates that no aggregate risk is to be used")
        options.addOption(Opts.NOAGGALT, false, "Indicates that no aggregate risk is to be used")
        options.addOption(Opts.NOEXIT, false, "Indicates that firms do not exit or enter")
        options.addOption(Opts.TRANSITION, false, "Indicates that a perfect foresight transition" +
          " path is to be calculated")
        options.addOption(Opts.HIGHTAX, false, "Indicates that the solution should be" +
          "under the assumption of the highe tax rate calculated")
          
      }

    def createConfig(cl: CommandLine): Config = {

      val config = new Config()

      if (cl.hasOption(Opts.INITDIST)) {
        config.setInitialSimState(new DiscretisedDistribution(
          new File(cl.getOptionValue(Opts.INITDIST))))
      }
      
      config.exit = !cl.hasOption(Opts.NOEXIT)

      config._aggUncertainty = !(cl.hasOption(Opts.NOAGG) || cl.hasOption(Opts.NOAGGALT))
      config._transition = cl.hasOption(Opts.TRANSITION)
      
      val maxFirmSize = 20

      // The only individual state is firm size
      config.setIndividualEndogenousStates(nLogSequence(maxFirmSize, 301, 1))

      config.setIndividualEndogenousStatesForSimulation(nLogSequence(maxFirmSize, 2001, 0))

      //    config.setIndividualExogenousStates(stocProc.getLevels().map(exp))
      val nIndStates = 11
//      val sigma = Math.sqrt((.085^2)/(1d-(.99^2)))
//      val stocProc = new DiscreteStochasticProcessFactory().tauchen(nIndStates, .99, sigma, 2.1295952/sigma)
     val stocProc = new DiscreteStochasticProcessFactory().rouwenhorst(nIndStates, .99, .085)
      val indTrans = stocProc.getTransitionProbabilities()

      config.setIndividualExogenousStates(stocProc.getLevels().map(exp))

      var trans: DoubleArray = indTrans.copy()

      1 to 10 foreach { _ => trans = trans %* trans }

      config.firmDemandLevelWeights = trans(0, $).copy

      //config.setIndividualExogenousStates(createArray(.5,1,10))

      val highTax = cl.hasOption(Opts.HIGHTAX)

      //      val indTrans = createArrayOfSize(3, 3) << (.99,.01,0d,
      //                                                  .0025,.99,.0075,
      //                                                    0.0025,.0075,.99);
      config.s = 0.0081
      config.δ = 0.99^(1d/12)
      
      config.c2 = 6
      config.c2r = 4
      config.β = .052
      config.z = .955

      val (l, v, v2, ybyL, entry, entryVal) = if (highTax) {
        SteadyStates.highTax
      
      } else {
        SteadyStates.lowTax
      }

      //      val L = 0.8152312489829352
      //      val V = 0.1589578217340476
      //      val V2 = 0.07307939401086624
      //      val YbyL = 3.208961375027624
      //      val Entry = 0.009682495

      val u = 1d - l
      //

      val entryCost = if (highTax) 7.407183 else 98.62317

      val taxRate = if (highTax) .4348 else .4248

      if (cl.hasOption(Opts.TRANSITION)) {
        val proportionBad = .7

        config.expectedL = l

        //        val probStayBad = 1d - 1d / 104
        //  
        //        val probStayGood = (1 - 2 * proportionBad + proportionBad * probStayBad) / (1 - proportionBad)
        //        indTrans << (probStayBad, 1 - probStayBad, 1 - probStayGood, probStayGood)

        config.setAggregateEndogenousStates(
          //Employment   
          createArray(l),

          // Output relative to employment
          createArray(ybyL))

        // The only aggregate exo state is the tax rate
        config.setAggregateExogenousStates(createArray(taxRate))

        // V and V^2
        config.setAggregateControls(
          createArray(v),
          createArray(v2))

        val aggTrans = createArrayOfSize(1, 1)
        aggTrans += 1

        //      val trans = combineTransitions(
        //        stocProc.getTransitionProbabilities(), aggTrans
        val trans = combineTransitions(
          indTrans, aggTrans)

        config.setExogenousStateTransiton(trans)
        
      } else if (cl.hasOption(Opts.NOAGG)) {
        /**
         * No AGG RISK
         */
        val proportionBad = .7

        config.expectedL = l

        //        val probStayBad = 1d - 1d / 104
        //  
        //        val probStayGood = (1 - 2 * proportionBad + proportionBad * probStayBad) / (1 - proportionBad)
        //        indTrans << (probStayBad, 1 - probStayBad, 1 - probStayGood, probStayGood)

        config.setAggregateEndogenousStates(
          //Employment   
          createArray(Utils.sequence(l - .04, Math.min(l + .04, .99), 5): _*),

          // Output relative to employment
          createArray(ybyL))

        // The only aggregate exo state is the tax rate
        config.setAggregateExogenousStates(createArray(taxRate))

        // V and V^2
        config.setAggregateControls(
          createArray(v),
          createArray(v2))

        val aggTrans = createArrayOfSize(1, 1)
        aggTrans += 1

        //      val trans = combineTransitions(
        //        stocProc.getTransitionProbabilities(), aggTrans
        val trans = combineTransitions(
          indTrans, aggTrans)

        config.setExogenousStateTransiton(trans)
      } else if (cl.hasOption(Opts.NOAGGALT)) {
        /**
         * No AGG RISK
         */
        val proportionBad = .7
        config.expectedL = l

        //        val probStayBad = 1d - 1d / 104
        //  
        //        val probStayGood = (1 - 2 * proportionBad + proportionBad * probStayBad) / (1 - proportionBad)
        //        indTrans << (probStayBad, 1 - probStayBad, 1 - probStayGood, probStayGood)

        config.setAggregateEndogenousStates(
          //Employment   
          createArray(Utils.sequence(l - .04, Math.min(l + .04, .99), 5): _*),
          createArray(.9,1,1.1) * ybyL)
        // The only aggregate exo state is the tax rate
        config.setAggregateExogenousStates(createArray(taxRate))

        // V and V^2
        config.setAggregateControls(
          createArray(.9,1,1.1)*v,
          createArray(.9,1,1.1)*v2)

        val aggTrans = createArrayOfSize(1, 1)
        aggTrans += 1

        //      val trans = combineTransitions(
        //        stocProc.getTransitionProbabilities(), aggTrans
        val trans = combineTransitions(
          indTrans, aggTrans)

        config.setExogenousStateTransiton(trans)
      }
      else {
        
        
        /**
         * AGG RISK
         */
        config.setInitialSimState(
            new DiscretisedDistribution(new File("Solutions/SignUpAbs_NA", "lastSS.mat")))

        config.setAggregateEndogenousStates(
        		
          //Employment
          createArray(Utils.sequence(l - .04, Math.min(l + .04, .99), 5):_*),

          // Output relative to employment...
          createArray(.99, 1, 1.01) * ybyL)

        // The only aggregate exo state is productivity
        config.setAggregateExogenousStates(
          createArray(taxRate,.4348))

        val vArray = createArray(-.02, 0, .02) + v
//        val vArray = createArray(v)
        val v2Array = createArray(v2)
  
        config.setAggregateControls(
          vArray,
          v2Array)


        //      val trans = combineTransitions(
        //        stocProc.getTransitionProbabilities(), aggTrans
        val meanDuration = 24d
        val probStay = 1 - 1 / meanDuration

        val aggTrans = createArrayOfSize(2, 2)
        aggTrans << (probStay, 1 - probStay, 1 - probStay, probStay)

        //val indTrans = createArrayOfSize(2, 2)

        val trans = createArrayOfSize(11, 2, 11, 2)
        trans \ (0, 2) << indTrans

        trans \ (1, 3) *= aggTrans
        
        val sanity = (trans\(2,3)).sum
        
        config.setExogenousStateTransiton(trans)
        config.setControlsAffectingExpectations(0)
      }

      config
    }

    def getModelClass(): Class[_ <: Model] = classOf[Model]
  }

  class Model extends AbstractModelWithControls[Config, State] {

    var simEmpGrid: DoubleArray = _
    var simFirmOutput: DoubleArray = _

    var fullEmpGrid: DoubleArray = _
    var aggQ: DoubleArray = _
    var fullQ: DoubleArray = _
    var fullInvQ: DoubleArray = _

    var fullϰ: DoubleArray = _

    var aggYOp: Double => Double = _
    var invAggYOp: Double => Double = _

    var netRevenue: DoubleArray = _
    var wage: DoubleArray = _

    var employmentGrid : DoubleArray = _

    override def initialise() = {
      super.initialise()
      employmentGrid = createIndividualVariableGrid(1)\0 << _config.firmEmploymentLevels
      val q = createArrayOfSize(_config.aggregateEmploymentLevels.numberOfElements,
        _config.aggregateVLevels.numberOfElements())
      q \ (1) << _config.aggregateVLevels
      q \ (0) /= (1d - _config.aggregateEmploymentLevels)
      q ->= (_config.matchingFn.vacancyFillingRate(_))

      // Fill an aggregate grid with appropriate q values
      aggQ = createAggregateVariableGrid(1)
      aggQ \ (0, 2) << q

      // Fill an individual grid with appropriate q values
      fullQ = createIndividualVariableGrid(1)
      fullQ \ (2, 4) << q

      fullInvQ = 1d / fullQ

      fullEmpGrid = createIndividualVariableGrid(1)
      fullEmpGrid \ (0) << _config.firmEmploymentLevels

      wage = createIndividualVariableGrid(1)

      // val wage = z + β * c2 * V2 / ((1-β)*(1 - L))

      // First fill it with the wage, which is shared across firms
      wage \ 5 << (_config.c2 * _config.β / (1 - _config.β) * _config.aggregateV2Levels)
      wage \ 2 /= ((1d - _config.aggregateEmploymentLevels))
      wage += _config.z

      netRevenue = createIndividualVariableGrid(1)

      val prices = createIndividualVariableGrid(1)

      // The part depending on individual firm output
      prices \ 0 << ((_config.P * _config.firmEmploymentLevels + _config.ɸ) ^ (-1d / _config.γ))
      // Times the demand and productivity factors
      prices \ 1 *= (_config.P * _config.firmDemandLevels)
      // Times the part depending on aggregate output (REMEMBER: The agg is actually Y over L
      prices \ 3 *= (_config.aggregateOutputLevels ^ (1d / _config.γ))
      prices \ 2 *= (_config.aggregateEmploymentLevels ^ (1d / _config.γ))

      // Price per sold unit and productivity+...
      netRevenue = prices \ 6 * (_config.P * (1d - _config.taxLevels))
      // ... minus wage per un1t
      netRevenue -= wage
      // ... times labour units
      netRevenue \ 0 *= _config.firmEmploymentLevels

      simEmpGrid = createSimulationGrid() \ (0) << _config.firmEmploymentLevelsForSimulation

      val dixitStiglitzPower = 1 - 1 / _config.γ

      // Each firm's output contribution before aggregation. The 0 case may occur for the overflow

      simFirmOutput = createSimulationGrid()
      simFirmOutput \ (0) << ((_config.P * _config.firmEmploymentLevelsForSimulation +
        _config.ɸ) ^ dixitStiglitzPower)
      simFirmOutput \ (1) *= _config.firmDemandLevels

      /* OUTPUT (DIXIT-STIGLITZ AGGREGATE!)
       */

      aggYOp = _ ^ (1d / dixitStiglitzPower)
      invAggYOp = _ ^ (dixitStiglitzPower)
    }

    def initialState(): State = {
      val state = new State(_config)

      state.employmentGrid = employmentGrid
      
      val simEmpGridSize = employmentGrid.size().clone()
      simEmpGridSize(0) = _config.firmEmploymentLevelsForSimulation.numberOfElements

      state.simEmploymentGrid = createArrayOfSize(simEmpGridSize: _*)
      state.simEmploymentGrid \ (0) << _config.firmEmploymentLevelsForSimulation

      /* Aggregate State Expectations
       */
      //      val expectedAggStates = createAggregateExpectationGrid(2)
      //      expectedAggStates \ (0) << _config.aggregateEmploymentLevels
      //      state.setExpectedAggregateStates(expectedAggStates)

      initAggregateTransition(state)

      /* Individual transition !
       */
      val trans = createIndividualTransitionGrid(1)
      trans \ 0 << (_config.firmEmploymentLevels -> ((x: Double) => ((x - .1) * .9).max(0)))

      state.setIndividualPolicy(trans)

      /* Individual Control Expectations
       */
      state.setIndividualControlsPolicy(createIndividualVariableGrid(1))
      state.firmRedundanciesPolicy = createIndividualVariableGrid(1)

      state.futureBonus = createIndividualVariableGrid(1) << (_config.z * 1.1)

      state.setEndOfPeriodStates(createSimulationGrid() \ (0) <<
        _config.firmEmploymentLevelsForSimulation)

      // Make sure aggregate expecations are calculated
      adjustExpectedAggregates(state)

      return state
    }

    def median(array: DoubleArray) = array(array.numberOfElements / 2)

    def initAggregateTransition(state: State) = {
      val aggTrans = createAggregateVariableGrid(2)

      if (!_config.hasAggUncertainty) {
        aggTrans.lastDimSlice(Indexes.AggStates.L) += _config.expectedL
        // with no agg uncertainty the assumption is fixed by the fixed-point mechanism

        aggTrans.lastDimSlice(Indexes.AggStates.Y) \ (1) << _config.aggregateOutputLevels

      } else {
//        if (state.getAggregateTransition() != null) {
//          aggTrans.across(5) << (_config.aggregateEmploymentLevels(2),_config.aggregateOutputLevels(1))
//        } else{

          // Can't because forecast does not depend on V!
//          val lPrime = aggTrans.lastDimSlice(Indexes.AggStates.L)
//
//          val theta = median(_config.aggregateVLevels)/(1d-median(_config.aggregateEmploymentLevels))
//          val q = _config.matchingFn.vacancyFillingRate(theta)
//          val impSS_R = q *median(_config.aggregateVLevels) -_config.s * median(_config.aggregateEmploymentLevels)
//          
//          lPrime \ (2) << _config.aggregateVLevels
//          lPrime *= aggQ
//          lPrime \ (0) += _config.aggregateEmploymentLevels * (1d - _config.s)
//          lPrime -= impSS_R
            aggTrans.lastDimSlice(Indexes.AggStates.L)\2 << _config.aggregateVLevels
            aggTrans.lastDimSlice(Indexes.AggStates.L) *= aggQ
            
            aggTrans.lastDimSlice(Indexes.AggStates.L) \ (0) +=
                _config.aggregateEmploymentLevels * (1d-_config.s)

            aggTrans.lastDimSlice(Indexes.AggStates.L) -= 
              (aggTrans.lastDimSlice(Indexes.AggStates.L).sum()/aggTrans.lastDimSlice(Indexes.AggStates.L).numberOfElements()
                  - median(_config.aggregateEmploymentLevels))
   
          aggTrans.lastDimSlice(Indexes.AggStates.Y) \ (1) <<
            squeezeToMean(_config.aggregateOutputLevels, .2)
        //}
      }

      state.setAggregateTransition(aggTrans)
      val adjuster = createArray(Utils.sequence(1.1, .9, _config.aggregateEmploymentLevels.numberOfElements()):_*)

      /* Set each agg control to its lowest value initially
       */
      val aggControlsPolicy = createAggregateVariableGrid(2)

      if (state.getCurrentControlsPolicy() != null) {
        aggControlsPolicy \ (0, 5) << state.getCurrentControlsPolicy()($, 0, 0, 0, 0,  $)
      } else {
        
        aggControlsPolicy.lastDimSlice(Indexes.AggControls.V)\(0) << adjuster*median(_config.aggregateVLevels)
        aggControlsPolicy.lastDimSlice(Indexes.AggControls.V2)\(0) << adjuster*median(_config.aggregateV2Levels)
      }
      state.setCurrentControlsPolicy(aggControlsPolicy)

      /* Aggregate control Expectations
       */
      val expectedAggControls = createAggregateExpectationGrid(2)

      expectedAggControls.lastDimSlice(Indexes.AggControls.V) << median(_config.aggregateVLevels)
      expectedAggControls.lastDimSlice(Indexes.AggControls.V2) << median(_config.aggregateV2Levels)

      state.setExpectedAggregateControls(expectedAggControls)
    }

    override def convergenceFailed(state: State) {

      debugWriteArray(
        listToStackedArray(getIndividualSolverInstance().asInstanceOf[SimpleQuadraticCostsSolver].policyHistory),
        "history")

    }

    def convergeTo[T](target: Double, initial: T, metric: (T, T) => Double)(trans: T => T): T = {

      var current = initial
      var metricVal = Double.PositiveInfinity

      while (metricVal > target) {
        var next = trans(current)
        metricVal = metric(current, next)
        current = next
      }

      current
    }

    /**
     * Given the value and the benefits accruing from a single period, essentially performs
     * one iteration of Value Function iteration: Calculates the expectedValue from the current
     * value and adds that to the single period benefits to get the new value
     */
    def addSinglePeriodValue(totalValue: DoubleArray,
      singlePeriodValue: DoubleArray,
      state: State): DoubleArray = {

      
      val expectedFirmValue = calculateExpectedFirmValue(totalValue, state)

      val v = state.getIndividualControlsPolicy()

      val sizeIter = expectedFirmValue.iteratorAcross(Array(0))
      sizeIter.nextDouble()
      val size0Iter = sizeIter.getOrthogonalIterator

      // If size 0 firm does not grow then it exits, so expected value is 0
      (expectedFirmValue(0,$)::v(0,$)) =-> ((ei, vi) => if(vi < 1e-10) 0d else ei)
      
      // Calculate the new value function by using this expectation along with current net
      // profits
      singlePeriodValue + _config.δ * expectedFirmValue
    }

    /**
     * Given the current firm value function, calculates the expected one
     */
    def calculateExpectedFirmValue(currentValue: DoubleArray, state: State): DoubleArray = {
      val indSolver = getIndividualSolverInstance.asInstanceOf[SimpleQuadraticCostsSolver]
      // Calculate the value at on-grid individual statesand expected future aggregate states
      var condExpValue = conditionalExpectation(currentValue, state)

      // Fill a conditional expectations array with it
      var condFutureVal = indSolver.createFutureConditionalContributionsGrid()
      condFutureVal << condExpValue.arrangeDimensions(Array(0, 5, 1, 2, 3, 4, 6))

      // Using these conditional expectations, calculate unconditional ones (i.e. apply probs)
      val valAtLpOnGrid = indSolver.individualUnconditionalExpectation(condFutureVal, state)
      
      interpolateFunction(fullEmpGrid, valAtLpOnGrid, 0, state.getIndividualPolicy())

    }

    /**
     * Calculates the benefits accruing from a single period, conditional on shocks and aggregates
     */
    def calculatePeriodValue(state: State): DoubleArray = {

      val v = state.getIndividualControlsPolicy()

      val redundancyCosts = (state.firmRedundanciesPolicy ^ 2) * _config.c2r / 2d
      val vacancyPostingCosts = (v ^ 2) * _config.c2 / 2d

      (state.futureBonus ~ vacancyPostingCosts) =-> ((b, v) => if (v > 0) b else 0)

      val periodRevMinusCosts = netRevenue - redundancyCosts - vacancyPostingCosts -
        // Need to consider the sign-up bonuses which are determined this period but paid next!
        (_config.δ * state.futureBonus * v * fullQ)

      debugWriteArray(periodRevMinusCosts, "periodRev")

      periodRevMinusCosts
    }

    /**
     * Calculates the firms' value function, and stores it in the state
     */
    def calculateFirmValue(state: State, precision: Double = 1e-6) {

      val periodRevMinusCosts = calculatePeriodValue(state)

      println("Calculating Firm Value")

      val initVal = if (state.firmValue != null) state.firmValue else createIndividualVariableGrid()

      state.firmValue =
        convergeTo[DoubleArray](precision, initVal,(a:DoubleArray, b:DoubleArray)=>
          maximumRelativeDifferenceSpecial(a(10,$), b(10,$)))(currentValue => {
            addSinglePeriodValue(currentValue, periodRevMinusCosts, state)
          })

      // Calculate the (numerical) derivative of the value function, which should be 
      // the same as lambda
      state.impliedLambda = _config.δ * gradWeigthedHarmonic(state.firmValue,
        _config.firmEmploymentLevels, 0)

      // Calculates the value of entering the market
      state.entryValue = (state.firmValue(0, $,
        _config.aggregateEmploymentLevels.numberOfElements / 2, 0, 0, 0, 0, 0, 0) *
        _config.firmDemandLevelWeights).sum()

    }

    override def writeAdditional(state: State, writer: NumericsWriter) {
      super.writeAdditional(state, writer)
      writer.writeArray("expIndCtrls", state.expectedIndividualControls)
      writer.writeArray("indRedundanciesPolicy", state.firmRedundanciesPolicy)
      
      writer.writeArray("indRedundanciesPolicySim", interpolateFunction(employmentGrid,
            state.firmRedundanciesPolicy,
            0,
            state.simEmploymentGrid,
            params.constrained))
            
      if(state.firmValue != null) {            
        writer.writeArray("firmValue", state.firmValue)
        writer.writeArray("firmValueSim", interpolateFunction(employmentGrid,
              state.firmValue,
              0,
              state.simEmploymentGrid,
              params.constrained))
      }

      writer.writeArray("signupBonus", state.futureBonus)
      writer.writeArray("signupBonusSim", interpolateFunction(employmentGrid,
            state.futureBonus,
            0,
            state.simEmploymentGrid,
            params.constrained))
            
      writer.writeArray("meanFirmDemandWeights", _config.firmDemandLevelWeights)
      writer.writeArray("entryValue", createArray(state.entryValue))
      writer.writeArray("exitProportion", createArray(state.exitProportion))
      writer.writeArray("impliedLambda", state.impliedLambda)

      state.fpVals.foreach { fpVals => writer.writeArray("fixedPoint", fpVals) }

      if (getConfig().hasAggUncertainty()) {
        state.gradSimStates(0).write(writer, "finalGradDensity")
        state.gradSimStates(1).write(writer, "finalGradDensity_1")

        writer.writeStructure("grads0", state.gradData(0))
        writer.writeStructure("grads1", state.gradData(1))

      }
    }

    override def readAdditional(state: State, reader: NumericsReader) {

      
      // If the policy that has been read has only one shock level it is an NA state - need to expand
      if (!_config._transition && 
          _config.hasAggUncertainty && 
          state.getIndividualPolicy().size()(6) == 1) {

        state.setCurrentControlsPolicy(
          createAggregateVariableGrid(2) \ (5) <<
            reader.getArray[java.lang.Double]("aggCtrlsPolicy").asInstanceOf[DoubleArray](
              2d, 0, 0, 0, 0, $))

        val indPolicy = createIndividualTransitionGrid()
        indPolicy.across(0, 1) << state.getIndividualPolicy()($, $, 2)

        state.setIndividualPolicy(indPolicy)

        val indControlsPolicy = createIndividualVariableGrid(1)
        val naIndCtrls = reader.getArray[java.lang.Double]("indCtrlsPolicy").asInstanceOf[DoubleArray]
        indControlsPolicy.across(0, 1) << naIndCtrls($, $, 2)

        state.setIndividualControlsPolicy(indControlsPolicy)

        val indRedundanciesPolicy = createIndividualVariableGrid(1)
        val naIndRedund = reader.getArray[java.lang.Double]("indRedundanciesPolicy").asInstanceOf[DoubleArray]
        indRedundanciesPolicy.across(0, 1) << naIndRedund($, $, 2)
        println(indRedundanciesPolicy.max)
        println(indRedundanciesPolicy.min)
        
        state.firmRedundanciesPolicy = indRedundanciesPolicy

        val firmValue = createIndividualVariableGrid(1)
        val naFirmValue = reader.getArray[java.lang.Double]("firmValue").asInstanceOf[DoubleArray]
        firmValue \ (0, 1) << naFirmValue($, $, 2)
        //        firmValue \ 5 *= createArray(1.01, 1, .99)
        state.firmValue = firmValue

        // Also need to initialise the aggregate transition, because that will be the NA (constant) transition
        initAggregateTransition(state)

        // Calculate expectations from the aggregate transition
        adjustExpectedAggregates(state)
      } else {
        state.setIndividualControlsPolicy(reader.getArray[java.lang.Double]("indCtrlsPolicy").asInstanceOf[DoubleArray])
        state.expectedIndividualControls = reader.getArray[java.lang.Double]("expIndCtrls").asInstanceOf[DoubleArray]
        state.firmRedundanciesPolicy = reader.getArray[java.lang.Double]("indRedundanciesPolicy").asInstanceOf[DoubleArray]
        state.setCurrentControlsPolicy(reader.getArray[java.lang.Double]("aggCtrlsPolicy").asInstanceOf[DoubleArray])
        state.firmValue = reader.getArray[java.lang.Double]("firmValue").asInstanceOf[DoubleArray]

        // Calculate expectations from the aggregate transition
        adjustExpectedAggregates(state)
//
//        try {
//          var gradSimState = new DiscretisedDistribution()
//          gradSimState.read(reader, "finalGradDensity")
//
//          // Make sure the simulation starts from the same distribution used for derivative aggregation
//          _config.setInitialSimState(gradSimState)
//
//          println("Simulation will start from last grad sim state")
//        } catch {
//          case e: Exception => {
//            println("No grad density read")
//          }
//        }
      }
    }

    override def calculateAggregateStates(
      simState: SimState,
      currentAggShock: IntegerArray,
      state: State): Array[Double] = {

      val distribution = simState.asInstanceOf[DiscretisedDistribution]

      var L = distribution.mean(_config.firmEmploymentLevelsForSimulation)
      val Y = aggYOp(sumAcrossDistribution(simFirmOutput, simEmpGrid, distribution)(0))

      val YbyL = Y / L

      if (L < _config.aggregateEmploymentLevels.first || L > _config.aggregateEmploymentLevels.last) {
        //        println(s"L out of bounds (${_config.aggregateEmploymentLevels.first}, ${_config.aggregateEmploymentLevels.last}: ${L}")
      }

      if (_config.hasAggUncertainty && (YbyL < _config.aggregateOutputLevels.first || YbyL > _config.aggregateOutputLevels.last)) {
        //        println(s"Y out of bounds (${_config.aggregateOutputLevels.first}, ${_config.aggregateOutputLevels.last}: ${YbyL}")
      }

      Array(L, YbyL)
    }
    
    var lastEntry = 0d

    override def calculateControlDeterminants(simState: SimState,
      indTransByControl: JavaDoubleArray,
      aggStates: Array[Double],
      currentAggShock: IntegerArray,
      state: State): DoubleArray =
      {
        val distribution = simState.asInstanceOf[DiscretisedDistribution]

        val L = aggStates(0)
        val YbyL = aggStates(1)

        // Get the individual controls decisions, still conditional on aggregate controls but not
        // on aggregate states, which are known
        val indV = interp(state.getIndividualControlsPolicyForSimulation()($, $, $, $, $, $, 
          currentAggShock.get(0).doubleValue(), Indexes.IndControls.vacancies),
          spec(2, _config.aggregateEmploymentLevels, L),
          spec(3, _config.aggregateOutputLevels, YbyL))
          
        val V = sumAcrossDistribution(indV, simEmpGrid, distribution)
        val V2 = sumAcrossDistribution(indV ^ 2, simEmpGrid, distribution)

        if(!_config.hasAggUncertainty()) {
          return createArray(V(0),V2(0))
        }
        val entryLevels = SteadyStates.lowTax._5 * createArray(0,1,2)
        
        
        val entryVX = createArrayOfSize(11,3,3,1)
        entryVX\(0,1) << indV(0, $,$,0)
        
        // Entry is done at the beginning prior to (aggregate) shock realisation - it was in effect
        // calculated the prior sim period
        // The vacancies posted by size-0 (i.e. entering) firms
        entryVX\2 *= entryLevels
        // Weighted by their entry distribution
        entryVX\0 *= _config.firmDemandLevelWeights

        // The 1 is for the firm shock dimension, which is still present
        // Scaled to the total amount of entry
        //entryV \ (Indexes.AggControls.Entry + 1) *= _config.entryLevels

        val entrySums = (entryVX \ (0) sum ())\0 + V($,0)
        val v2WithEntry = ((entryVX ^ 2)\0 sum)\0 + V2($,0)

        // These are the actual V's resulting from indidvidual choice and entry
        val vVals = entrySums($,$,0)
        // These are the observed Vs that drove those choices
        val vTargets = createArrayOfSize(vVals.size:_*)\0 << _config.aggregateVLevels
        
        // Match them up, conditional on entry
        val vSolutions = interpolateFixedPoints(vVals, vTargets, 0, 0)

        
        // This gives the firm entry values at on-grid V levels!
        val entryValues: DoubleArray = if (state.firmValue != null) {
          val firm0Values = interp(state.firmValue(0, $, $, $, $, $, 
            currentAggShock.get(0).doubleValue(), 0),
            spec(1, _config.aggregateEmploymentLevels, L),
            spec(2, _config.aggregateOutputLevels, YbyL))

          (firm0Values \ (0) * _config.firmDemandLevelWeights) \ (0) sum ()
        } else {
          createArrayOfSize(V.size: _*) << median(entryLevels)
        }
        // Stack the arrays together in the appropriate order
        //V.stack(entryValues, V2)
        val entryVals = interpolateFunction(_config.aggregateVLevels, entryValues($,0), 0, vSolutions)
      
        val equilibriumEntryImplied = Interpolation.interpDimension(entryLevels, entryVals - SteadyStates.lowTax._6, 0, 0)(0)
        
        val equilibriumEntry = equilibriumEntryImplied.max(0)
        
        val equilibriumV = Interpolation.interpDimension(vSolutions, entryLevels,equilibriumEntry,0)(0)
        
        distribution._density(0,$) << equilibriumEntry * _config.firmDemandLevelWeights
        val equilibriumV2 = interp(v2WithEntry,
            spec(0, _config.aggregateVLevels, equilibriumV),
            spec(1, entryLevels, equilibriumEntry))(0)
            
            lastEntry = equilibriumEntry
            
        createArray(equilibriumV, equilibriumV2)
      }
    
//    override protected def calcControls(determinants: JavaDoubleArray, controlGrid: Array[JavaDoubleArray], targets: Array[JavaDoubleArray]) : Array[Double] = {
//      val vVals = determinants($,$,0)
//      val vTargets = createArrayOfSize(vVals.size:_*)\0 << targets(0)
//      
//      val vSolutions = interpolateFixedPoints(vVals, vTargets, 0, 0)
//      
//      val entryVals = determinants($,$,1)
////      val entryValsAtV = createArrayOfSize(vSolutions.size:_*)
////      
////      for(i <- 1 until vSolutions.numberOfElements) {
////        entryValsAtV = Interpolation.interpSingle(x$1, x$2, x$3, x$4, x$5, x$6)Dimension(x$1, x$2, x$3, x$4) 
////      }
//      val entryValsAtV = interpolateFunction(vTargets, entryVals, 0, createArrayOfSize(1,3) << vSolutions).transpose(0, 1)($,0).copy()
//      
//      val equilibriumEntry = Interpolation.interpDimension(controlGrid(1), entryValsAtV.transpose(0, 1) - targets(1), 0, 0)(0)
//      
//      val equilibriumV = Interpolation.interpDimension(vSolutions, controlGrid(1), equilibriumEntry, 0)(0)
//      
//      
//      Array(equilibriumV, equilibriumEntry)
//    }
	

    override protected def afterControlsCalculated[N <: Number](controls: Array[Double],
      currentAggStates: Array[Double],
      priorAggShockIndices: MultiDimensionalArray[N, _],
      calcState: State): Unit =
      {
        val V = controls(0)

        if (_config.hasAggUncertainty &&
          (V < _config.aggregateVLevels.first || V > _config.aggregateVLevels.last)) {
          //          println(s"V out of bounds (${_config.aggregateVLevels.first}, " +
          //            s" ${_config.aggregateVLevels.last}: ${V}")
        }
      }

    override def beforeSimInterpolation[S <: SimState](dist: S,
      record: TransitionRecord[_ <: SimState, _ <: Number],
      state: State) {

      if (!_config.hasAggUncertainty()) return

      var currentDist = dist.asInstanceOf[DiscretisedDistribution]

      var totalEntry = lastEntry

      // Perform the entry based on the calculated amount
      currentDist._density(0, $) << (_config.firmDemandLevelWeights * totalEntry)
}

    override def afterSimInterpolation(record: TransitionRecord[_ <: SimState, _ <: Number],
      state: State) {

      if(_config.exit) {
        var dist = record.getResultingDistribution.asInstanceOf[DiscretisedDistribution]
        var exiters = dist._density(0, $)
  
        val totalExit = exiters.sum()
  
        state.exitProportion = totalExit
  
        //      if(_config.hasAggUncertainty()) {
        //        // The size of the firm population is changing
        //        record.setExpectedPopulation(record.getExpectedPopulation() - totalExit)
        //            
        //            // Clear the exiters!
        //            exiters << 0
        //      } else {
  
        // Replace the exiters with the same number of firms but distributed according to the steady
        // state prob dist of the firm shock process
        if(!_config.hasAggUncertainty()) {
          dist._density(0, $) << (_config.firmDemandLevelWeights * totalExit)
        } else {
          // Make the exit! New firms will enter next period
          dist._density(0, $) << 0
          record.setExpectedPopulation(record.getExpectedPopulation() - totalExit)
        }
  
      }
    }

    override def getFixedPointDelegate(): FixedPointValueDelegate[AggregateFixedPointState[State, _ <: com.meliorbis.economics.model.Model[Config, State]]] = {

      if(_config.getAggregateControls().get(0).numberOfElements() > 1) {
        return super.getFixedPointDelegate()
      }
      
      val inner = new ModelAndConfigValueDelegate[Config, State, Model](_config).
        asInstanceOf[FixedPointValueDelegate[AggregateFixedPointState[State, _ <: com.meliorbis.economics.model.Model[Config, State]]]]

      new FixedPointValueDelegate[AggregateFixedPointState[State, _ <: Model]]() {

        override def getBounds(): Array[Array[Double]] = {
          var bounds = new Array[Array[Double]](4)

          // Only 1-L is bounded
          bounds(0) = Array(Math.log(.02), 0)

          bounds
        }

        override def setInputs(inputs: Array[Double]): Unit = {

          val U = Math.exp(inputs(0))
          val L = 1 - U
          val Y = Math.exp(inputs(1))
          val V = Math.exp(inputs(2)) * U
          val V2 = Math.exp(inputs(3))

          println(s"In L: ${L}, Y/L: ${Y}, V: ${V}, V2: ${V2}")

          _config.expectedL = L

          _config.getAggregateEndogenousStates().get(Indexes.AggStates.L) <<
            createArray(Utils.sequence(L - .04, Math.min(L + .04, .99),
              _config.getAggregateEndogenousStates().get(Indexes.AggStates.L).numberOfElements()): _*)

          _config.getAggregateEndogenousStates().get(Indexes.AggStates.Y)(0) = Y

          _config.getAggregateControls().get(Indexes.AggControls.V)(0) = V
          _config.getAggregateControls().get(Indexes.AggControls.V2)(0) = V2
        }

        override def getOutputs(state: AggregateFixedPointState[State, _ <: Model]): Array[Double] = {
          val orig = inner.getOutputs(state)

          val L = orig(0)
          val U = 1 - L
          val Y = orig(1)
          val V = orig(2)
          val V2 = orig(3)

          println(s"Out L: ${L}, Y/L: ${Y}, θ: ${V / U}, V: ${V}, V2: ${V2}")

          state.getCalcState().fpVals = Some(createArray(U, Y, V, V2))

          Array(Math.log(U), Math.log(Y), Math.log(V / U), Math.log(V2))
        }

        override def solutionFound(state: AggregateFixedPointState[State, _ <: Model]): Unit = {

          state.getModel().calculateFirmValue(state.getCalcState())
        }

        override def getInitialInputs(): Array[Double] = {

          val orig = inner.getInitialInputs()

          val U = 1 - _config.expectedL
          val Y = orig(1)
          val V = orig(2)
          val V2 = orig(3)

          Array(Math.log(U), Math.log(Y), Math.log(V / U), Math.log(V2))
        }
      }.asInstanceOf[FixedPointValueDelegate[AggregateFixedPointState[State, _ <: com.meliorbis.economics.model.Model[Config, State]]]]

    }

    def shouldUpdateAggregates(state: State): Boolean = /*state.getPeriod > 50 */state.getIndividualCriterion.getValue < 1e-6
  }

  class Config extends ModelConfigBase {

    var matchingFn = new DRWMatchingFunction(0.407)
    var γ: Double = 5
    var β: Double = .4
    var δ: Double = .98
    var ɸ: Double = .001

    var s: Double = 0.001
    var P: Double = 1

    var c2: Double = 1
    var c2r: Double = 1

    var z: Double = .7

    var expectedL: Double = _

    var firmDemandLevelWeights: DoubleArray = _

    var _aggUncertainty = true
    var _transition = false
    var exit = true
    
    setInitialExogenousStates(createIntArray(0))

    override def hasAggUncertainty(): Boolean = _aggUncertainty

    override def getAggregateProblemSolver() = classOf[MatchingAggSolver]

    override def getIndividualSolver() = classOf[SimpleQuadraticCostsSolver]

    def isConstrained(): Boolean = true

    override def readParameters(src: NumericsReader): Unit = {

    }

    override def writeParameters(tgt: NumericsWriter): Unit = {

    }

    override def getInitialSimState(): SimState = {

      val superVal = super.getInitialSimState()

      if (superVal != null) {
        return superVal
      }

      val simState = new DiscretisedDistribution()

      var indTrans = getExogenousStateTransition()($, 0, $, 0)

      for (i <- 1 to 10) {
        indTrans = indTrans %* indTrans
      }

      simState._density = createArrayOfSize(
        firmEmploymentLevelsForSimulation.numberOfElements(),
        firmDemandLevels.numberOfElements())

      simState._overflowAverages = createArrayOfSize(1, firmDemandLevels.numberOfElements())
      simState._overflowProportions = createArrayOfSize(1, firmDemandLevels.numberOfElements())

      var idx = 0
      while (firmEmploymentLevelsForSimulation(idx) <
        aggregateEmploymentLevels.sum() / aggregateEmploymentLevels.numberOfElements()) {
        idx = idx + 1
      }

      val minEmp = Math.max(10, idx - 50)

      for (empLevel <- minEmp until (minEmp + 100)) {
        simState._density(empLevel, $) << indTrans(0, $) / 100
      }

      simState
    }

    /* Provide readable access to the configured variables
     */

    def firmEmploymentLevels = getIndividualEndogenousStates().get(0)
    def firmDemandLevels = getIndividualExogenousStates().get(0)

    def firmEmploymentLevelsForSimulation = getIndividualEndogenousStatesForSimulation().get(0)

    def aggregateEmploymentLevels = getAggregateEndogenousStates().get(0)
    def aggregateOutputLevels = getAggregateEndogenousStates().get(Indexes.AggStates.Y)

    def taxLevels = getAggregateExogenousStates().get(0)

    def aggregateVLevels = getAggregateControls().get(Indexes.AggControls.V)
    def aggregateV2Levels = getAggregateControls().get(Indexes.AggControls.V2)

    // def aggregateRedundancyLevels = getAggregateControls().get(Indexes.AggControls.R)

  }

  class State(config: Config) extends AbstractStateWithControls[Config](config)
      with DerivAggCalcState[Config] {

    var indError = 1.0
    var aggError = if (config.hasAggUncertainty) 1d else 0d
    var ctrlError = 1.0
    var expectedIndividualControls: DoubleArray = _
    var firmRedundanciesPolicy: DoubleArray = _
    var expectedFirmRedundancies: DoubleArray = _
    var futureBonus: DoubleArray = _
    var firmValue: DoubleArray = _
    var impliedLambda: DoubleArray = _

    var entryValue: Double = Double.NaN
    var exitProportion: Double = Double.NaN

    var employmentGrid: DoubleArray = _
    var simEmploymentGrid: DoubleArray = _

    var fpVals: Option[DoubleArray] = None

    val gradData: Array[Map[String, MultiDimensionalArray[_ <: Number, _]]] = new Array(2)

    val gradSimStates: Array[DiscretisedDistribution] =
      Array.fill(config.taxLevels.numberOfElements) {
        config.getInitialSimState().asInstanceOf[DiscretisedDistribution].clone()
      }

    def getDistributionForState(shocks: Array[Int]): DiscretisedDistribution = {
      gradSimStates(shocks(0))
    }

    def setDerivatives(shocks: Array[Int],
      current: JavaDoubleArray,
      future: JavaDoubleArray,
      grads: JavaDoubleArray*): Unit = {

      gradData(shocks(0)) = Map("current" -> current, "future" -> future, "firstOrder" -> grads(0))
      //,"secondOrder" -> grads(1))
    }

    override def getIndividualPolicyForSimulation(): DoubleArray = {

      updateSimPolicies()

      // Need to update the policy for the simulaiton grid, which is used by derivative aggregation
      return _individualTransitionForSimulation
    }

    private def updateSimPolicies() {

      if (_individualTransitionForSimulation == null) {
        _individualTransitionForSimulation =
          interpolateFunction(employmentGrid,
            getIndividualPolicy(),
            0,
            simEmploymentGrid,
            params.constrained)

        super.setIndividualControlsPolicyForSimulation(
          interpolateFunction(employmentGrid,
            getIndividualControlsPolicy(),
            0,
            simEmploymentGrid,
            params.constrained))
      }
    }

    override def getIndividualControlsPolicyForSimulation(): DoubleArray = {
      updateSimPolicies()

      super.getIndividualControlsPolicyForSimulation()
    }
  }

  object Indexes {

    object AggStates {
      val L = 0
      val Y = 1
    }

    object AggControls {
      val V = 0
      val V2 = 1
    }

    object IndControls {
      val vacancies = 0
      val w = 1
    }

  }

  /**
   * Solves the individual problem in the case of quadratic costs with no reservation wage and no
   * entry or exit
   */
  class SimpleQuadraticCostsSolver(model: Model, config: Config)
      extends EGMIndividualProblemSolverWithControls[Config, State, Model](model, config) {

    import config._
    import model._

    val lForMV = ((P * firmEmploymentLevels + ɸ) ^ (-1 / γ - 1)) *
      ((1 - 1 / γ) * P * firmEmploymentLevels + ɸ)

    var expectationProvider: ExpectationProvider = new ExpectationProviderUnderUncertainty

    /* Calculates the array of bonuses from the vs
     */
    val bonus = {
      val bonusFactor = ((β * c2) / ((1 - β) * δ)) * fullInvQ

      (v: DoubleArray) => v * bonusFactor
    }
    
    // No damping during transition path, because we are calculating each policy directly
    val newWeight = if(config._transition) 1d else if(config._aggUncertainty) .5d else .1d

    /* Set up functions to go from lambda to v and vice versa
     */
    val (vFromLambda, lambdaFromPosDelta) = {

      // This factor turns a positive lambda*q into v
      val vFactor = (1 - β) / ((1 + β) * c2)

      // This function takes q and lambda and returns v
      ((q: Double, lambda: Double) => vFactor * q * lambda,

        // This function accepts q and returns a function which accepts the change in lp from l and
        // will calculate the implied lambda

        (q: Double) => {
          // Have to divide by q twice - once to get v from l' - (1-s)l, and once to get to lambda
          val factor = 1 / (vFactor * q * q)

          (lpdelta: Double) => factor * lpdelta
        })
    }

    // Make sure the two functions are inverses!
    assert(lambdaFromPosDelta(0.1)(vFromLambda(0.1, 0.1) * 0.1) === .1 +- 1e-15,
      s"${lambdaFromPosDelta(0.1)(vFromLambda(0.1, 0.1) * 0.1)} should be .1")

    /* Set up functions to go from negative lambda to r and vice versa
     */
    val (rFromLambda, lambdaFromNegDelta) = {

      val c2rInv = 1 / c2r

      ((lambda: Double) => -lambda * c2rInv,
        (r: Double) => r * c2r)
    }

    assert(rFromLambda(lambdaFromNegDelta(-0.1)) === .1 +- 1e-15,
      s"${rFromLambda(lambdaFromNegDelta(-0.1))} should be .1")

    setStartOfPeriodStates(employmentGrid)
    setEndOfPeriodStates(employmentGrid)
    
      // Expose
      override def createFutureConditionalContributionsGrid() = 
        super.createFutureConditionalContributionsGrid()
    /**
     * Given expected double-future marginal values and the wage, calculates the marginal value
     */
    def calculateFutureConditionalExpectations(
      exoTransition: Array[Int],
      currentAggIdx: Array[Int],
      state: State): DoubleArray = {

      // Output is expressed relative to employment, so reconstruct it
      val L = expectationProvider.getExpectedAggregateState(exoTransition, currentAggIdx,
        state, Indexes.AggStates.L)

      val YbyL = expectationProvider.getExpectedAggregateState(exoTransition, currentAggIdx, state,
        Indexes.AggStates.Y)

      // Output (turned into consumption below as part of Kappa calc)
      val Y = YbyL * L

      val V = expectationProvider.getExpectedAggregateControl(exoTransition, currentAggIdx, state,
        Indexes.AggControls.V)

      val V2 = expectationProvider.getExpectedAggregateControl(exoTransition, currentAggIdx, state,
        Indexes.AggControls.V2)

      val q = matchingFn.vacancyFillingRate(L, V)

      // Individual expected controls
      val lpp = expectationProvider.getExpectedIndividualTransition(exoTransition, currentAggIdx,
        state)

      val lAdj = _config.firmEmploymentLevels * (1 - s)

      // Future tax level
      val tau = taxLevels(exoTransition(3))

      // Future firm productivity
      val epsilon = firmDemandLevels(exoTransition(2))

      // The 'revenue factor' for the firm
      val ϰ = P * epsilon * (1 - tau) * (Y ^ (1d / γ))

      val wage = z + β * c2 * V2 / ((1 - β) * (1 - L))

      val lpdeltaToLambda = lambdaFromPosDelta(q)

      val mv = (lAdj :: lForMV :: lpp) -> ((lMinusS, lPow, lpp) => {

        // Calculate the firm's endogenous adjustment
        val lpDelta = lpp - lMinusS

        // Implied future 
        val λprime = if (lpDelta > 0) {

          lpdeltaToLambda(lpDelta)

        } else {
          // Note that this also covers the special case where lpDelta = 0 - then because v=r=0 the
          // inequality constraints imply lambda = 0. This need not correspond to MV if l'= 0 too.
          // (Which implies l = 0).

          // Note that r = lpdelta!
          lambdaFromNegDelta(lpDelta)
        }

        // Marginal value
        δ * (ϰ * lPow - wage + (1 - s) * λprime)
      })

      if(diff(mv,0).max > 0) {
        debugWriteArray(diff(mv,0), "indmv")
        println("Ind mv not decreasing")
      }
      mv
    }

    override def individualUnconditionalExpectation(condFutureVal: JavaDoubleArray, state: State): JavaDoubleArray = {
      super.individualUnconditionalExpectation(condFutureVal, state)
    }

    def calculateImpliedStartOfPeriodState(individualExpectationsIN: JavaDoubleArray, state: State): DoubleArray = {

      val individualExpectations: DoubleArray = individualExpectationsIN.asInstanceOf[DoubleArray]

//      var b = state.futureBonus
//      val l = firmEmploymentLevels

      val mvprime = individualExpectations.lastDimSlice(0)

      debugWriteArray(mvprime, "lambda")
   
      if (diff(mvprime, 0).max() > 0) {
        
        debugWriteArray(gradWeigthedHarmonic(mvprime, _config.firmEmploymentLevels, 0), "lambdagrad")
        println(s"""lambda increasing: Period ${state.getPeriod}""")
      }

      val oneMinusSInv = 1 / (1 - s)

      val Array(impliedR, impliedV, impliedL) = (mvprime :: employmentGrid :: fullQ) :-> (
          (mvprime, lprime, q) => {
  
            if (mvprime < 0) {
              // Expected marginal value is smaller than 0, by implication the firm must be making
              // employees redundant  
              val r = rFromLambda(mvprime)
              val impL = (lprime + r) * oneMinusSInv
  
              Array(r, 0d, impL)
            } else {
              // Expected marginal value is greater than 0, by implication the firm must be hiring
              // Also covers = 0, which means v = 0
              val v = vFromLambda(q, mvprime)
  
              val impL = (lprime - q * v) * oneMinusSInv
              Array(0d, v, impL)
            }
        })

      debugWriteArray(impliedV, "impV")
      debugWriteArray(impliedL, "impL")

      impliedL
    }

    var oldCtrls: DoubleArray = _
    var oldRedundancies: DoubleArray = _

    var policyHistory: ListBuffer[DoubleArray] = ListBuffer[DoubleArray]()

    override def updateError(
      oldPolicy: JavaDoubleArray,
      newPolicy: JavaDoubleArray,
      state: State) {
      if(state.getPeriod % 10 == 5) {
        state.setIndividualError(maximumRelativeDifferenceSpecial(newPolicy, oldPolicy))
      }
    }

    override def afterPolicyUpdate(
      oldPolicy: JavaDoubleArray,
      newPolicy: JavaDoubleArray,
      state: State) {

      
      if (newPolicy.min() < 0) {
        println(s"""Negative lp: Period ${state.getPeriod}""")
      }

      if (diff(newPolicy, 0).min() < 0) {
        println(s"""Non-monotonous policy: Period ${state.getPeriod}""")
      }

      // do the damping
      //(newPolicy :: oldPolicy) =-> (_ * newWeight + _ * (1d-newWeight))

      debugWriteArray(newPolicy, "lp")

      // Re-update the policy with the damped one
      state.setIndividualPolicy(newPolicy)
      
      // now calc v or r from the policy
      val lpDiff = newPolicy - (1 - s) * fullEmpGrid

      val Array(v, r) = (lpDiff :: _model.fullInvQ) :-> ((d, qInv) => d match {
          case x if x > 0 => Array(d*qInv, 0)
          case _ => Array(0, -d)
        })

      state.setIndividualControlsPolicy(v)
      state.firmRedundanciesPolicy = r

      state.futureBonus = bonus(v)

      debugWriteArray(fullInvQ, "qInv")
      debugWriteArray(fullQ, "q")
      debugWriteArray(v, "v")
      debugWriteArray(r, "r")

      

//      if (policyHistory.isEmpty)
//        policyHistory += oldPolicy
//
//      if (policyHistory.length == 100)
//        policyHistory = policyHistory.tail
//
//      policyHistory += newPolicy

      //      debugWriteArray(
      //          listToStackedArray(getIndividualSolverInstance().
      //              asInstanceOf[SimpleQuadraticCostsSolver].policyHistory.toList),
      //          "history")
      // Clear the policy for simulation
      state.setIndividualControlsPolicyForSimulation(null)

      //    debugWriteArray(newPolicy, "l")
    }


    
  }

  class MatchingAggSolver(model: Model, config: Config, sim: DiscretisedDistributionSimulator) 
  extends KrusellSmithSolver[Config, State, Model, DiscretisedDistribution](model, config, sim) {

    import model._

    setNewWeight(.1)
    keepDist(true)
    reuseShocks(true)
    setSimPeriods(600)
    setDiscardPeriods(100)
    
    addTransitionListener((oldVal: DoubleArray, newVal: DoubleArray, state: State) => {
      state.aggError = maximumRelativeDifference(oldVal, newVal)
      state.ctrlError = 0d //maximumRelativeDifferenceSpecial(oldCtrls, state.getCurrentControlsPolicy())

      debugWriteArray(newVal, "aggTrans")
      debugWriteArray(state.getCurrentControlsPolicy, "aggCtrls")
    })
    override def prepareAggregatePolicyCalculation(state: State) {

      val oldVal = state.firmValue

     model.calculateFirmValue(state,1e-4)

      val diff = maximumRelativeDifferenceSpecial(oldVal, state.firmValue)

      println(s"""Value Diff: ${diff}""")
    }
  }

    class MatchingDASolver(model: Model, config: Config, 
        sim: DiscretisedDistributionSimulator)
        extends DerivativeAggregationSolver[Config, State, Model](model, config, sim) {
  
      import config._
      import model._
  
      // Use a log linear rather than a linear transition
      useLogs(false)
  
      secondOrder(false)
      setNewWeight(.01)
  
      // This grid needs to be the size of the simulation grid
      val ones = model.createSimulationGrid()
      ones << 1
  
      val v2Deriv = model.createSimulationGrid()
      v2Deriv \ 0 << config.firmEmploymentLevelsForSimulation
      v2Deriv *= 2
  
      val zeros = model.createSimulationGrid()
  
      val thetaDeriv = model.createSimulationGrid()
      thetaDeriv \ (0) << config.firmEmploymentLevelsForSimulation
  
      val dYdl = model.createSimulationGrid()
      dYdl \ (0) << ((firmEmploymentLevelsForSimulation + ɸ) ^ (-1 / γ))
      dYdl \ (1) *= (P * config.firmDemandLevels)
  
      val d2Ydl2 = model.createSimulationGrid()
      d2Ydl2 \ (0) << (1 - 1 / γ) * (-1 / γ) * (config.firmEmploymentLevelsForSimulation ^ (-1 / γ - 1))
      d2Ydl2 \ (1) *= (P * config.firmDemandLevels)
  
      addTransitionListener((oldVal: DoubleArray, newVal: DoubleArray, state: State) => {
        state.aggError = maximumRelativeDifference(oldVal, newVal)
        state.ctrlError = maximumRelativeDifferenceSpecial(oldCtrls, state.getCurrentControlsPolicy())
  
        debugWriteArray(state.gradSimStates(0)._density, "dens0")
        debugWriteArray(state.gradSimStates(1)._density, "dens1")
  
        debugWriteArray(newVal, "aggTrans")
        debugWriteArray(state.getCurrentControlsPolicy, "aggCtrls")
  
        //      state.expectedV2 = model.sumAcrossDistribution(
        //        state.expectedIndividualControls.arrangeDimensions(Array(4, 5, 0, 1, 2, 3, 6)) ^ 2,
        //        model.smallEmpGrid,
        //        state.getDistributionForState(Array(0)))
      })
  
      var oldCtrls: DoubleArray = _
  
      override def prepareAggregatePolicyCalculation(state: State) {
  
        debugWriteArray( state.getIndividualPolicy, "indpol")
        oldCtrls = state.getCurrentControlsPolicy()
  
        if (state.firmValue == null) {
          state.firmValue = calculatePeriodValue(state)
        }
  
        val oldVal = state.firmValue
  
        //val singlePeriodValue = calculatePeriodValue(state)
  
        calculateFirmValue(state, 1e-5)
        //(1 to 10).foreach(_ => state.firmValue = addSinglePeriodValue(state.firmValue, singlePeriodValue, state))
  
        val diff = maximumRelativeDifferenceSpecial(oldVal, state.firmValue)
  
  //      if(state.getPeriod() % 10 == 5) {
          println(s"""Value Diff: ${diff}""")
//        }
        debugWriteArray( state.firmValue, "value")
        
        /* Need to update the distributions for calculating grad here
         */
  
        // Don't do this in parallel as the starting point of successive states is the initial point of the last
        for (level <- 0 until taxLevels.numberOfElements()) {
          updateGradDensity(level, state)
        }
      }
  
      def shouldUpdateDensity(state: State): Boolean = false//state.getPeriod % 10 == 0
  
      def updateGradDensity(level: Int, state: State) {
  
        if (!shouldUpdateDensity(state)) {
          return
        }
  
        val meanDuration = 24
        val initialRun = 2 //meanDuration/2
  
        val startingProdLevel =
          (if (level == 0) taxLevels.numberOfElements else level) - 1
  
        val startingDist = state.gradSimStates(startingProdLevel)
  
        // Expected duration of good/bad periods in this calibration
        // Note that the first period is the 'starting' period and does not count
        val periods = 3 * meanDuration + 1;
  
        var shockArray = createIntArrayOfSize(periods, 1)
        var fillArray = createIntArrayOfSize(periods).add(level.asInstanceOf[Integer])
  
        // Set the shock to be simulated to the level the density was requested for
        shockArray.at(-1, 0).fill(fillArray)
  
        // The first 4 should be the other shock state
        for (j <- 0 until initialRun) {
          shockArray.set(startingProdLevel.asInstanceOf[Integer], j, 0)
        }
  
        // Then alternatingly 8 periods each until the last for, which are this shock state
        for (j <- 0 until (periods / meanDuration)) {
          if (j % 2 == 1) {
            for (i <- j * meanDuration + initialRun until (j + 1) * meanDuration + initialRun) {
              shockArray.set(startingProdLevel.asInstanceOf[Integer], i, 0)
            }
          }
        }
  
        val simResults = getSimulator().
          asInstanceOf[DiscretisedDistributionSimulator].simulateShocks(
            startingDist,
            shockArray,
            _model,
            state,
            SimulationObserver.silent[DiscretisedDistribution, Integer]())
  
        state.gradSimStates(level) = simResults.getFinalState()
      }
  
      override def deriveAggregationByAggregateState(aggregationIndex: Int, stateIndex: Int, aggExoStates: Array[Int], aggregateStates: Array[Double]): DoubleArray = {
  
        // Impact of L on aggregation for (Y/L)^(1-1/gamma)
        val L = aggregateStates(0)
        val deriv = ((1 / γ - 1) * (L ^ (1 / γ - 2)))
  
        // d(firmOutput/L^(1-1/gamma))/dL
        model.simFirmOutput * deriv
      }
  
      override def deriveDeterminantAggregationByAggregateState(aggregationIndex: Int, stateIndex: Int, aggExoStates: Array[Int], aggregateStates: Array[Double]): DoubleArray = {
        // None of the states affect the controls	explicitly
        zeros
      }
  
      override def deriveDeterminantAggregationByIndividualControl(
        aggregationIndex: Int,
        indControlIndex: Int,
        aggExoStates: Array[Int],
        aggregateStates: Array[Double],
        controlPolicy: JavaDoubleArray): DoubleArray = {
  
        aggregationIndex match {
          case Indexes.AggControls.V => ones
  
  //        case Indexes.AggControls.Entry => zeros
  
          case Indexes.AggControls.V2 => v2Deriv
  
          case _ => throw new IllegalArgumentException(
            "aggregation requested for non-existent variables!")
        }
  
      }
  
      override def deriveAggregationByIndividualState(
        aggIndex: Int,
        indIndex: Int,
        aggShocks: Array[Int],
        aggStates: Array[Double], current: Boolean): DoubleArray = {
  
        aggIndex match {
          case Indexes.AggStates.L => ones
          case Indexes.AggStates.Y => (aggStates(Indexes.AggStates.Y) ^ (1 / γ)) / aggStates(0) * dYdl
        }
  
      }
  
      override def deriveIndividualTransformationByTheta(
        aggIndex: Int,
        indIndex: Int,
        aggShocks: Array[Int],
        aggStates: Array[Double]): DoubleArray = {
  
        aggIndex match {
          case Indexes.AggStates.L => model.simEmpGrid // increases proportional to firm size
          case Indexes.AggStates.Y => model.simEmpGrid - aggStates(0) // moves away from mean at
          // rate proportional to distance        
        }
      }
  
      override def doubleDeriveAggregationByIndividualStates(
        aggIndex: Int,
        indIndex: Int,
        indIndex2: Int,
        aggShocks: Array[Int],
        aggStates: Array[Double]): DoubleArray = {
        if (aggIndex == 0) { // Impact of l on L
          zeros
        } else { // Impact of l on Y/L
          d2Ydl2 / model.invAggYOp(aggStates(0))
        }
      }
  
      override def doubleDeriveAggregationByIndividualAndAggregateState(aggIndex: Int,
        indIndex: Int,
        aggIndex2: Int,
        aggShocks: Array[Int],
        aggStates: Array[Double]): DoubleArray = {
        val L = aggStates(0)
        val deriv = ((1 / γ - 1) * (L ^ (1 / γ - 2)))
  
        dYdl * deriv
      }
  
      override def doubleDeriveAggregationByAggregateStates(aggregationIndex: Int,
        aggIndex: Int,
        aggIndex2: Int,
        aggShocks: Array[Int],
        aggStates: Array[Double]): DoubleArray = {
        // Impact of L on aggregation for (Y/L)^(1-1/gamma)
        val L = aggStates(0)
        val deriv = ((1 / γ - 1) * (1 / γ - 2) * (L ^ (1 / γ - 3)))
  
        // d(firmOutput/L^(1-1/gamma))/dL
        model.simFirmOutput * deriv
      }
  
      override def doubleDeriveIndividualTransformationByTheta(
        transformationIndex: Int,
        transformation2Index: Int,
        individualIndex: Int,
        aggExoIndex: Array[Int],
        currentAggregates: Array[Double]): DoubleArray = {
        zeros
      }
    }
}