package com.meliorbis.economics.matching.quadraticCosts.demandShocks

import com.meliorbis.numerics.scala.DoubleArray._

trait ExpectationProvider {
  def getExpectedAggregateState(exoTrans: Array[Int], currentAggs: Array[Int], 
      state: SignUpAbsolute.State, stateIndex: Int) : Double
 
  def getExpectedAggregateControl(exoTrans: Array[Int], currentAggs: Array[Int], 
      state: SignUpAbsolute.State, stateIndex: Int) : Double
      
  def getExpectedIndividualTransition(exoTrans: Array[Int], currentAggs: Array[Int], 
      state: SignUpAbsolute.State) : DoubleArray
}