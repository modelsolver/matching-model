package com.meliorbis.economics.matching.quadraticCosts.demandShocks

import com.meliorbis.numerics.scala.DoubleArray._
import com.meliorbis.numerics.DoubleArrayFactories._

class ExpectationProviderUnderUncertainty extends ExpectationProvider {
  
  def getExpectedAggregateState(exoTrans: Array[Int], currentAggs: Array[Int], 
      state: SignUpAbsolute.State, stateIndex: Int) : Double = {
     getExpectedAggregateVariable(exoTrans, currentAggs, stateIndex,
         state.getExpectedAggregateStates)
  }
 
  def getExpectedAggregateControl(exoTrans: Array[Int], currentAggs: Array[Int], 
      state: SignUpAbsolute.State, controlIndex: Int) : Double = {
    getExpectedAggregateVariable(exoTrans, currentAggs, controlIndex,
        state.getExpectedAggregateControls)
  }
 
  def getExpectedIndividualTransition(exoTrans: Array[Int], currentAggs: Array[Int], 
      state: SignUpAbsolute.State) : DoubleArray = {
    getExpectedValue(exoTrans, currentAggs,state.getExpectedIndividualTransition)
  }
  
  private def getExpectedAggregateVariable(transitionIndex: Array[Int],
      aggStateIndex: Array[Int],
      variableIndex: Int,
      expectations: DoubleArray): Double = {
      val currentAggStateIndex = transitionIndex(1)
      val futureAggStateIndex = transitionIndex(3)

      val aggLabourIndex = aggStateIndex(0)
      val aggOutputIndex = aggStateIndex(1)

      return expectations.get(
        currentAggStateIndex,
        futureAggStateIndex,
        aggLabourIndex,
        aggOutputIndex,
        variableIndex)
    }
  
  private def getExpectedValue(
      transitionIndex: Array[Int],
      aggDetStateIndex: Array[Int],
      conditionalExpectations: DoubleArray): DoubleArray = {

      val currentAggProductivityIndex = transitionIndex(1)
      val futureIndProductivityIndex = transitionIndex(2)
      val futureAggProductivityIndex = transitionIndex(3)

      val aggLabourIndex = aggDetStateIndex(0)
      val aggOutputIndex = aggDetStateIndex(1)

      conditionalExpectations.at(
        currentAggProductivityIndex,
        futureAggProductivityIndex,
        aggLabourIndex,
        aggOutputIndex,
        -1,
        futureIndProductivityIndex,
        /* return all expected controls!*/
        -1)
    }
}