package com.meliorbis.economics.matching

import org.scalatest.junit.AssertionsForJUnit
import org.junit.Test
import com.meliorbis.economics.matching.function.DRWMatchingFunction
import org.scalatest.Matchers._

class DRWMatchingFunctionTest extends AssertionsForJUnit {
  
  val DELTA = 1e-10
  
  @Test def testRelations() : Unit = {
    val fn = new DRWMatchingFunction(.4)
    
    val L = .945
    val V = .05
    
    val m = fn.matches(L, V)
    val q = fn.vacancyFillingRate(L, V)
    val q2 = fn.vacancyFillingRate(V/(1-L))
    val f = fn.jobFindingRate(L, V)
    val f2 = fn.jobFindingRate(V/(1-L))
    
    q should equal (q2 +- DELTA)
    f should equal (f2 +- DELTA)
    
    m/V should equal (q2 +- DELTA)
    m/(1-L) should equal (f2 +- DELTA)
  }
}