package com.meliorbis.economics.matching

import com.meliorbis.numerics.scala.DoubleArray._
import org.junit.Test
import com.meliorbis.numerics.DoubleNumerics
import org.junit.Before
import org.junit.Assert._
import com.meliorbis.numerics.generic.primitives.impl.DoubleArrayFunctions
import com.meliorbis.numerics.generic.primitives.impl.DoubleArrayFunctions._
import com.meliorbis.economics.matching.function.DRWMatchingFunction
import com.meliorbis.utils.Utils
import scala.reflect.internal.util.Collections
import java.util.Collections
import com.meliorbis.numerics.test.ArrayAssert

class MatchingEGMSolverTests {
//
//  val ALLOWED_ERROR = 1e-15
//    
//  val _numerics = new DoubleNumerics
//  val _model = new Model()
//  val _config = new Config()
//  
//  DoubleArrayFunctions.init(_numerics)
//  
//  _config.setIndividualEndogenousStates(nLogSequence(1d, 20, 0))
//  _config.setIndividualExogenousStates(_numerics.getArrayFactory().newArray(1d,2d))
//  _config.setAggregateExogenousStates(_numerics.getArrayFactory().newArray(1d))
//  _config.setAggregateControls(_numerics.getArrayFactory().newArray(1d),_numerics.getArrayFactory().newArray(.1d))
//  _config.setAggregateEndogenousStates(_numerics.getArrayFactory().newArray(.9d))
//  _config._aggregateOfferedWage = _numerics.getArrayFactory().newArray(.9d)
//  
//  _config._matchingFunction = new DRWMatchingFunction(.5)
//  _model.setNumerics(_numerics)
//  
//  _model.setConfig(_config)
//  _model.initialise()
//  val _solver = new MatchingEndogenousGridSolverMinL(_model,_config)
//  
//  _solver.setNumerics(_numerics)
//  
//  @Test def testConstantFindsNothing() {
//    
//    val testArray = _numerics.getArrayFactory().newArray(20,2,1,1)
//    
//    val regions = _solver.findNonMonotonousRegions(testArray)
//    
//    assertEquals(2,regions.length)
//    assertEquals(0,regions(0).length)
//    assertEquals(0,regions(1).length)
//  }
//  
//  @Test def testMonotonousFindsNothing() {
//    
//    val testArray = _numerics.getArrayFactory().newArray(20,2,1,1)
//    
//    testArray($,0) << Utils.sequence(1d, 10d, 20)
//    testArray($,1) << Utils.sequence(1d, -10d, 20)
//    
//    val regions = _solver.findNonMonotonousRegions(testArray)
//    
//    assertEquals(2,regions.length)
//    assertEquals(0,regions(0).length)
//    assertEquals(0,regions(1).length)
//  }
//  
//  @Test def testNonMonotonousFindsSomething() {
//    
//    val testArray = _numerics.getArrayFactory().newArray(20,2,1,1)
//    
//    testArray($,0) << Utils.sequence(1d, 10d, 20)
//    
//    testArray(10,0,0,0) = (testArray(11,0,0,0) + testArray(12,0,0,0))/2
//    
//    testArray($,1) << Utils.sequence(1d, -10d, 20)
//    
//    val regions = _solver.findNonMonotonousRegions(testArray)
//    
//    assertEquals(2,regions.length)
//    
//    // only one element out of place, so only one region...
//    assertEquals(1,regions(0).length)
//    
//    // No elements out of place, so no region...
//    assertEquals(0,regions(1).length)
//    
//    // lowest element at 11...
//    assertEquals(testArray(11,0,0,0), regions(0).head._start, ALLOWED_ERROR)    
//    // and highest element at 12
//    assertEquals(testArray(12,0,0,0),regions(0).head._end, ALLOWED_ERROR)    
//    
//    assertEquals(0,regions(1).length)
//  }
//  
//  @Test def testFixNonMonSections() {
//    
//    val lambda = _numerics.getArrayFactory().newArray(20,2,1,1)
//    
//    lambda($,0) << Utils.sequence(1d, -10d, 20)
//    
//    lambda(10,0,0,0) = (lambda(11,0,0,0) + lambda(12,0,0,0))/2
//    
//    lambda($,1) << Utils.sequence(1d, -10d, 20)
//    
//    val l = _numerics.getArrayFactory().newArray(20,2,1,1)
//    
//    l.fillDimensions(Utils.sequence(1d, 20d, 20),0)
//    
//    // Introduce some values in need of fixing.
//    l(10,0,0,0) = 100d
//    l(11,0,0,0) = 50d
//    
//    val nonMonSections = new Array[List[_solver.NonMonotonousSection]](2)
//    
//    // The second list should be empty
//    nonMonSections(1) = Nil
//    
//    nonMonSections(0) = List(new _solver.NonMonotonousSection(lambda(11,0,0,0), lambda(12,0,0,0)))
//    
//    val expectedLs = Array(100d, 100d, 100d, 50d, 50d, 50d, 13d, 13d, 13d)
//    val expectedAltIdxs = Array(10, 11, 12,11, 10, 12,12, 10, 11)
//    
//    val returnValues = Array(10d, 9d, 11d, 10d, 9d, 11d, 12d, 10d, 10d)
//    
//    var count = 0
//    
//    val actualLs = new Array[Double](9)
//    val actualIdxs = new Array[Int](9)
//    
//    val calcValue : ((Double, Array[Int], Array[Int]) => Double) = (l: Double, baseIdx: Array[Int], altIdx: Array[Int]) => {
//      
//      actualLs(count) = l
//      actualIdxs(count) = altIdx(0)
//      
//      count = count + 1
//      
//      // returning l will cause 12 to always be highest, so 10 and 11 will be nulled
//      returnValues(count-1)
//    }
//
//    val updateValue = (iter: IDoubleSubspaceSplitIterator, y1: Double, x1: Double, y2: Double, x2: Double) => {
//      val curX = _config.getFirmEmploymentLevels()(iter.getIndex()(0))
//      iter.set(y1 + (curX - x1)*(y2-y1)/(x2-x1))
//    }
//    
//    _solver.fixNonMonotonousRegions(lambda, l, nonMonSections, calcValue, updateValue)
//    
//    assertEquals(9, count)
//
//    assertArrayEquals(expectedLs, actualLs,ALLOWED_ERROR)
//    assertArrayEquals(expectedAltIdxs, actualIdxs)
//    
//     val expectedL = _numerics.getArrayFactory().newArray(20,2,1,1)
//    
//    expectedL.fillDimensions(Utils.sequence(1d, 20d, 20),0)
//    
//    ArrayAssert.assertEquals(expectedL, l, ALLOWED_ERROR)
//  }
}